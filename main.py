from utils import *


create_json(
    filename="price.csv",
    skeleton=skeleton_price,
    schema=priceRunner,
    order_columns_csv=order_columns_csv_price,
    translater=translater_price,
    filename_output="price"
)

create_json(
    filename="inventory1.csv",
    skeleton=skeleton_inventory,
    schema=inventoryRunner,
    order_columns_csv=order_columns_csv_inventory,
    translater=translater_inventory,
    filename_output="inventory"
)

create_json(
    filename="inventory2.csv",
    skeleton=skeleton_inventory,
    schema=inventoryRunner,
    order_columns_csv=order_columns_csv_inventory,
    translater=translater_inventory,
    filename_output="inventory"
)


    
        
        