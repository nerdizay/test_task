from loguru import logger

def debug_only(record):
    return record["level"].name == "DEBUG"

def info_only(record):
    return record["level"].name == "INFO"

def error_only(record):
    return record["level"].name == "ERROR"

logger.add("logs/debug.log", filter=debug_only)
logger.add("logs/info.log", filter=info_only)
logger.add("logs/error.log", filter=error_only)