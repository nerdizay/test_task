order_columns_csv_price = {
    0: "name",
    1: "categories",
    2: "price",
    3: "price_ext_id",
    4: "vat",
    5: "unit_type",
    6: "unit_ratio"
}

order_columns_csv_inventory = {
    0: "STORE_EXT_ID",
    1: "PRICE_EXT_ID",
    2: "SNSH_DATETIME",
    3: "IN_MATRIX",
    4: "QTY",
    5: "SELL_PRICE",
    6: "PRIME_COST",
    7: "MIN_STOCK_LEVEL",
    8: "STOCK_IN_DAYS",
    9: "IN_TRANSIT"
}