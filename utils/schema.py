from utils.custom_types import *

inventoryRunner = {
    "store_ext_id":{
        "type": str,
        "len": {
            "min": 1,
            "max": 40
        },
        "is_necessary": True
    },
    "price_ext_id":{
        "type": str,
        "len": {
            "min": 1,
            "max": 40
        },
        "is_necessary": True
    },
    "snapshot_datetime":{ #"SNSH_DATETIME"
        "type": datetime,
        "len": None,
        "is_necessary": True
    },
    "in_matrix":{
        "type": bool,
        "len": None,
        "is_necessary": False
    },
    "qty":{
        "type": float,
        "len": {
            "min": decimal_13_4_min,
            "max": decimal_13_4_max
        },
        "is_necessary": True
    },
    "sell_price":{
        "type": float,
        "len": {
            "min": decimal_12_4_min,
            "max": decimal_12_4_max
        },
        "is_necessary": False
    },
    "prime_cost":{
        "type": float,
        "len": {
            "min": decimal_12_4_min,
            "max": decimal_12_4_max
        },
        "is_necessary": False
    },
    "min_stock_level":{
        "type": float,
        "len": {
            "min": decimal_13_4_min,
            "max": decimal_13_4_max
        },
        "is_necessary": False
    },
    "stock_in_days":{
        "type": float,
        "len": {
            "min": smallint_min,
            "max": smallint_max
        },
        "is_necessary": False
    },
    "in_transit": {
        "type": float,
        "len": {
            "min": decimal_13_4_min,
            "max": decimal_13_4_max
        },
        "is_necessary": False
    }
}


priceRunner = {
    "name":{
        "type": str,
        "len": {
            "min": 1,
            "max": 200
        },
        "is_necessary": True
    },
    "categories": {
        "type": str,
        "len": {
            "min": 0,
            "max": infinity
        },
        "is_necessary": False
    },
    "price": {
        "type": float,
        "len": {
            "min": decimal_10_4_min,
            "max": decimal_10_4_max
        },
        "is_necessary": False
    },
    "extId": { #"price_ext_id"
        "type": str,
        "len": {
            "min": 1,
            "max": 40
        },
        "is_necessary": True
    },
    "vat": {
        "type": float,
        "len": {
            "min": decimal_4_2_min,
            "max": decimal_4_2_max
        },
        "is_necessary": False
    },
    "unit_type": {
        "type": unit_type,
        "len": None,
        "is_necessary": False
    },
    "unit_ratio": { #TODO: Уточнить, точно ли оно. items - количество унитарных единиц в единице товара
        # unit_ratio - (Опционально) Коэффициент, пакетное соотношение проданного количества к unit_type
        "type": float,
        "len": {
            "min": decimal_4_2_min,
            "max": decimal_4_2_max
        },
        "is_necessary": False
    }
}


