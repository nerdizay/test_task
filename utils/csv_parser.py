import csv
from utils.logger import logger
import time
import json
from pathlib import Path

from utils.validate import *

FOLDER_JSON = "json"
FOLDER_CSV = "csv"
    
    
def create_json(
    filename: str,
    skeleton: dict,
    schema: dict,
    order_columns_csv: dict,
    translater: dict,
    filename_output: str
) -> None:
    
    logger.info(f"Начали обрабатывать файл {filename}")

    json_items = []
    skeleton = skeleton.copy()
    count_columns = len(order_columns_csv)
    full_filename = Path(FOLDER_CSV, filename)
    full_filename_output = Path(FOLDER_JSON, filename_output)

    with open(full_filename, 'r', newline='', encoding="utf-8-sig") as csvfile:
        reader = csv.reader(csvfile, delimiter=',', escapechar='\\')

        for row in reader:
                
            validate_columns(row, count_columns)
            json_item = {}
            for index, elem in enumerate(row):
                name_column = order_columns_csv[index]
                name_column = translater.get(name_column, name_column.lower())
                try:
                    handled_value = None or validate(value=elem, name_column=name_column, schema=schema)
                except (ValidateError, Exception, BaseException) as e:
                    logger.error(
                        f"Ошибка при чтении файла {filename} \n"
                        f"Номер строки - {reader.line_num} \n"
                        f"Строка - {row} \n"
                        f"Название колонки - {name_column} "
                        f"Значение - {row[index]} \n"
                        f"{e}"
                    )
                    json_item = {}
                    break
                else:
                    value = handled_value or elem
                    if skeleton["method"] == "price.updateBatch":
                        if "params" not in json_item:
                            json_item["params"] = {}
                        if name_column != "name":
                            json_item["params"][name_column] = value
                    else:
                        json_item[name_column] = value
                        
            if json_item:
                if skeleton["method"] == "price.updateBatch":

                    json_item["params"]["units"] = {}
                    
                    _unit_type = json_item["params"].pop("unit_type")
                    unit_ratio = json_item["params"].pop("unit_ratio")
                    
                    handler_unit_type = unit_type.convert.get(_unit_type)
                    
                    if handler_unit_type:
                        try:
                            unit_ratio = float(unit_ratio)
                            key = unit_type.translater_units[_unit_type]
                            value = str(handler_unit_type(unit_ratio))
                            
                            json_item["params"]["units"][key] = value
                        except ValueError:
                            json_item["params"].pop("units")
                            
                elif skeleton["method"] == "inventoryUpdateBatch":
                    in_transit = json_item.pop("in_transit")
                    if in_transit:
                        d = datetime.now()
                        json_item["operations"] = {
                            "type": "move",
                            "qty": in_transit,
                            "ts": datetime.strftime(d, "%Y-%m-%d %H:%M")
                        }     
                    
            if json_item:
                json_items.append(json_item.copy())
                    
    
    skeleton["params"]["items"] = json_items
    if skeleton["params"]["items"]:
        with open(f"{full_filename_output}{time.time()}.json", "w", encoding="UTF-8") as file:
            file.write(json.dumps(skeleton, indent=4, default=str, ensure_ascii=False))