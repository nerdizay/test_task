from utils.custom_types import *
from utils.translater import translater_inventory, translater_price


class ValidateError(Exception):
    pass

def validate_str(
    value,
    name_column,
    _len,
    _type=None,
):
    if _len:
        if len(value) < _len["min"] or len(value) > _len["max"]:
            raise ValidateError(f"у {name_column} не может быть такая длина")
        
        
def validate_float(
    value,
    name_column,
    _type,
    _len
):
    if _len:
        if _type(value) < _len["min"] or len(value) > _len["max"]:
            raise ValidateError(f"{name_column} не должен быть > {_len['max']} и не должен быть < {_len['min']}")
        
        
def validate_datetime(
    value,
    name_column=None,
    _type=None,
    _len=None
):
    try:
        datetime.fromisoformat(value)
    except ValueError:
        raise ValidateError("Формат даты-времени не соответсвует ISO формату")
    
    
def validate_bool(
    value,
    name_column=None,
    _type=None,
    _len=None
):
    if value in ["1", "0", "true", "false"]:
        return
    else:
        raise ValidateError("boolean тип может принимать значения только: [\"1\", \"0\", \"true\", \"false\"]")
    
    
def validate_unit_type(
    value,
    name_column,
    _type=None,
    _len=None
):
    if value in unit_type:
        return
    else:
        raise ValidateError(f"{name_column} не может быть только одним из {str(unit_type)}")
    
    
def validate_columns(row, count_columns):
    if len(row) < count_columns:
        row.append('')
        validate_columns(row, count_columns)
    else:
        return
    

validate_choice = {
    str: validate_str,
    float: validate_float,
    bool: validate_bool,
    unit_type: validate_unit_type,
    datetime: validate_datetime
}
    

def validate(value, name_column, schema):
    
    possible_names = schema.keys()
    # possible_names = ["name", "categories", "price", "price_ext_id", "vat", "unit_type", "unit_ratio"]
    if name_column not in possible_names:
        raise Exception(f"name_column не может быть таким - {name_column}")
    
    _type = schema[name_column]["type"]
    _len = schema[name_column]["len"]
    is_necessary = schema[name_column]["is_necessary"]
    
    if not value and not is_necessary:
        return
    elif not value and is_necessary:
        raise ValidateError(f"{name_column} должен обязательно присутствовать")
    
    try:
        validate_choice[_type](value=value, name_column=name_column, _type=_type, _len=_len)
    except ValidateError as e:
        raise e
    
    if name_column == "categories":
        return value.split("&&")
    