from datetime import datetime

infinity = float("inf")

decimal_10_4_min = 0 #-214_748.3648
decimal_10_4_max = 214_748.3647

decimal_4_2_min = 0
decimal_4_2_max = 99.99

decimal_13_4_min = 0
decimal_13_4_max = 999_999_999.9999

decimal_12_4_min = 0
decimal_12_4_max = 99_999_999.9999

smallint_min = 0
smallint_max = 32.768
 

class UnitType:
    possible_values = ["1", "10", "11", "12", "20", "21", "30", "31"]
    # 1  - шт
    # 10 - литры
    # 11 - мл #TODO: перевести в литры
    # 12 - ДАЛ (декалитры)
    # 20 - кг
    # 21 - граммы #TODO: перевести в кг
    # 30 - метры
    # 31 - квадратные метры
    
    translater_units = {
        "1": "items",
        "10": "volume",
        "11": "volume",
        "12": "alc_volume",
        "20": "gross_weight", #TODO: Уточнить насчёт "net_weight"
        "21": "gross_weight",
        "30": "width",
        "31": "area"
    }
    
    ml_to_l = lambda v: v*000.1
    gr_to_kg = lambda m: m*000.1
    nothing = lambda x: x
    
    convert = {
        "1": nothing,
        "10": nothing,
        "11": ml_to_l,
        "12": nothing,
        "20": nothing,
        "21": gr_to_kg,
        "30": nothing,
        "31": nothing
    }

    def __contains__(self, value):
        if value in self.possible_values:
            return True
        else:
            return False
        
    def __str__(self):
        return str(self.possible_values)
        
unit_type = UnitType()